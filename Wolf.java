public class Wolf {

  public int age;
  public String furColor;
  public char gender;

  public void hunt() {

    if (this.gender == 'm') {

      System.out.println("I am the alpha wolf so I will go get the food for my pack!");

    }

    else if (this.gender == 'f') {

      System.out.println("I will let my husband, who is the alpha wolf, go do the hunting.");

    }

  }

  public void sleep() {

    System.out.println("ZZZzzz... ZZZzzz... ");
    System.out.println();
    System.out.println("Now I am rested.");

  }

}

