public class Application {

	public static void main(String[] args) {

		Wolf John = new Wolf();
		John.age = 13;
		John.gender = 'm';
		John.furColor = "black";

		Wolf Linda = new Wolf();
		Linda.age = 17;
		Linda.gender = 'f';
		Linda.furColor = "white";

		System.out.println("John's age: " + John.age + " John's gender: " + John.gender + " John's fur color: " + John.furColor);
		System.out.println("Linda's age: " + Linda.age + " Linda's gender: " + Linda.gender + " Linda's fur color: " + Linda.furColor);

		Linda.hunt();
		John.hunt();

		Wolf[] pack = new Wolf[3];
		
		pack[0] = John;
		pack[1] = Linda;

		pack[2] = new Wolf();

		pack[2].age = 9;
		pack[2].gender = 'f';
		pack[2].furColor = "red";

		System.out.println(pack[2].age);
		System.out.println(pack[2].gender);
		System.out.println(pack[2].furColor);	
	}
}
